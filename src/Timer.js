import React, { Component } from 'react';
import './Timer.less';

export default class Timer extends Component {
  constructor() {
    super();
    this.state = {
      timerId: 0,
      time: 0
    };
  }
  
  componentDidMount() {
    const timerId = setInterval(() => {
      this.setState({time: ++this.state.time});
    }, 1000);
    this.setState({timerId});
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId);
  }

  render() {
    return <h1 className='timer-style'>
      {this.state.time + ' ' + this.props.timeUnit}
    </h1>
  }
}