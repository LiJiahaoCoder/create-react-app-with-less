import React from 'react';
import logo from './logo.svg';
import Timer from './Timer';
import './App.css';

function App() {
  return (
    <div className="App">
     <Timer timeUnit="s" />
    </div>
  );
}

export default App;
